#include <QCoreApplication>
#include<QDebug>
#include<QVariant>
#include <iostream>
#include<QtMath>
#include<QVector>
using namespace std;

void Menu();
void BinToDec();
bool SayiBinaryMi(long sayi);
void ToDec(long int donusecekSayi);
void ToBin(long int donusecekSayi);
void DecToBin();



int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	cout<<"\t\t\t\tWelcome to Converter"<<endl;
	Menu();
	return a.exec();
}

void Menu()
{
	int secim;
	cout<<"\t1) Binary to Decimal "<<endl;
	cout<<"\t2) Decimal to Binary "<<endl;
	cout<<"\t0) Çıkış"<<endl;
	cin>>secim;

	switch (secim) {
	case 1:
		//binary to decimal
		BinToDec();
		break;
	case 2:
		// decimal to binary
		DecToBin();
		break;
	case 0:
		//çıkış
		exit(0);
		break;
	default:
		cout<<"\tYanlış giriş yaptınız!!!"<<endl;
		Menu();
		break;
	}

}


void BinToDec()
{
	long int bin;
	qDebug()<<"\t Bin to Dec"<<endl;
	qDebug()<<"\t Lütfen binary bir sayi giriniz:  "<<endl;
	cin>>bin;
	if(SayiBinaryMi(bin) && bin>-1)
	{
		ToDec(bin);
	}
	else
	{
		qDebug()<<"Girdiğiniz sayi binary değildir!!!"<<endl;
		qDebug()<<"\n\t1)Tekrar giris "<<endl;
		qDebug()<<"\t2)Bir üst menü "<<endl;
		qDebug()<<"\t0) Çıkış "<<endl;
		int giris;
		cin>>giris;

		switch (giris) {
		case 1:
				BinToDec();
			break;
		case 2:
				Menu();
			break;
		case 0:
				exit(0);
			break;
		default:
			qDebug()<<"YAnlış giriş yaptınız!!\n\n";
			qDebug()<<"\n\t1)Tekrar giris "<<endl;
			qDebug()<<"\t2)Bir üst menü "<<endl;
			qDebug()<<"\t0) Çıkış "<<endl;
			break;
		}


	}

}

bool SayiBinaryMi(long int  sayi)
{
	QString sayiString= QString::number(sayi);
	for(int i=0 ;i<sayiString.length(); i++)
	{
		int digit;
		digit=sayi%10;
		//qDebug()<<digit;
		if(digit>1)
		{
			return false;

		}

		sayi/=10;
	}
	return true;
}

void ToDec(long donusecekSayi)
{
	long int sayimiz=0;
	QString sayiString= QString::number(donusecekSayi);
	for(int i=0 ;i<sayiString.length(); i++)
	{
		int digit;
		digit= donusecekSayi%10;
		sayimiz+= (qPow(2,(i))*digit);
		donusecekSayi/=10;

	}
	qDebug()<<sayiString<<"===>>>"<<sayimiz;
	Menu();
}

void DecToBin()
{
	int dec,giris;
	qDebug()<<"\t Dec to Bin"<<endl;
	qDebug()<<"\t Lütfen decimal bir sayi giriniz:  "<<endl;
	cin>>dec;
	ToBin(dec);

	qDebug()<<"\n\t1)Tekrar giris "<<endl;
	qDebug()<<"\t2)Bir üst menü "<<endl;
	qDebug()<<"\t0) Çıkış "<<endl;
	cin>>giris;
	switch (giris) {
	case 1:
			DecToBin();
		break;
	case 2:
			Menu();
		break;
	case 0:
			exit(0);
		break;
	default:
		qDebug()<<"Yanlış giriş yaptınız!!\n\n";
		qDebug()<<"\n\t1)Tekrar giris "<<endl;
		qDebug()<<"\t2)Bir üst menü "<<endl;
		qDebug()<<"\t0) Çıkış "<<endl;
		break;
	}
}

void ToBin(long donusecekSayi)
{
	int dec=0,tmp;
	QVector<int> sonsayi;
	//QString sayi= QString::number(donusecekSayi);
	while (true)
	{
		if(donusecekSayi<2)
		{
			sonsayi.push_back(donusecekSayi);
			break;
		}
		tmp=donusecekSayi/2;
		sonsayi.push_back(donusecekSayi-tmp*2);
		donusecekSayi=tmp;

	}
	for(int i=0; i<sonsayi.count();i++)
	{
		dec+=(sonsayi[i]*qPow(10,i));
	}
	qDebug()<<dec;
}
